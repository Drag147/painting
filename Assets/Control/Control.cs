﻿using UnityEngine;
using UnityEngine.Experimental.UIElements;

public class Control : MonoBehaviour
{
    public float holdTime { get; set; }
    public MouseButton mouseButton { get; set; }
    public bool enableDragClick = true;
    private float nowTime = 0f;

    public bool StartClick = false;
    public bool Draged = false;
    public bool NotDraged = true;
       
    void Update()
    {
        if (enableDragClick)
        {
            if (Input.GetMouseButtonDown((int)mouseButton))
            {
                this.StartClick = true;
                this.Draged = false;
                this.NotDraged = true;
            }

            if (StartClick)
            {
                this.nowTime += Time.deltaTime;
            }

            if (StartClick && !Draged && this.nowTime >= this.holdTime)
            {
                this.StartClick = false;
                this.Draged = true;
                this.NotDraged = false;
            }

            if (Input.GetMouseButtonUp((int)mouseButton))
            {
                this.nowTime = 0f;
                this.StartClick = false;
                this.Draged = false;
                this.NotDraged = true;
            }
        }
    }
}
