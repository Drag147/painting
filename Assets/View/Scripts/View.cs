﻿using System.Collections.Generic;
using UnityEngine;

public interface View
{
    //Генерация ячеек на view
    void GenerateViewCells(Dictionary<long, int> colorsNeeded, int widthPicture, int heightPicture);

    //Бар с цветами
    void GenerateColorsBar(Dictionary<int, Color32> ColorsDict);

    //Клик по новому цвету
    void SelectColorShow(int numberColor);

    //Удаление текста при правильном закрашивании квадрата
    void ClearText(long numberCell);

    //Правильное закрашивание квадрата
    void FillRight(long numberCell, Color32 colorFill);

    //Неправильное закрашивание квадрата
    void FillFailer(long numberCell, Color32 colorFill);

    //Увеличение масштаба
    void ZoomPlus();

    //Уменьшение масштаба
    void ZoomMinus();
}
