﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

//Класс вьюхи сделанный на UI. При желании можно написать и любую другую View и по другому 2D и 3D
public class View2DUI : MonoBehaviour, View
{
    [SerializeField]
    //Префаб 1 ячейки
    private GameObject CellPrefab = null;
    [SerializeField]
    //Родитель, в который будут помещаться ячейки
    private GameObject ScrollViewContent = null;
    //Максимальный размер 1 ячейки при масштабировании
    private int MaxCellSize = 90;
    //Текущий размер 1 ячейки
    private int CellSizeNow = 50;
    //Минимальный размер 1 ячейки
    private int MinCellSize = 5;
    //Значение на которое будет изменяться размер ячейки при зуме
    private int DeltaSize = 5;
    //Минимальное значение ячейки, при котором нужно прятать номер цвета в ячейке
    private int MinForHideNumberColor = 20;
    //Показывается ли сейчас номер цвета в ячейке. Для включения отключения показа текста
    private bool ShowNumberColor = true;
    [SerializeField]
    //Префаб элемента палитры
    private GameObject ColorElementPrefab = null;
    [SerializeField]
    //Спрайт для выбранного цвета. Черные границы сейчас.
    private Sprite SelectedColorSprite = null;
    [SerializeField]
    //Дефолтный спрайт, который висит когда цвет не выбран
    private Sprite DefualtColorSprite = null;
    [SerializeField]
    //Родитель, в который будут помещаться цвета палитры
    private RectTransform ContentColorBar = null;

    [SerializeField]
    //Горизонтальный scroll bar
    private GameObject HorizontalScrollBar = null;
    [SerializeField]
    //Вертикальный scroll bar
    private GameObject VerticalScrollBar = null;

    [SerializeField]
    //Кнопка увеличения
    private GameObject ButtonZoomPlus = null;
    [SerializeField]
    //Кнопка Уменьшения
    private GameObject ButtonZoomMinus = null;
    [SerializeField]
    //Статистика правильно закрашенных пикселей верх право.
    private Text TextPixelStats = null;
    [SerializeField]
    //Уведомления, сейчас туда выводится только общее время по окончанию игры
    private Text TextNotify = null;

    [SerializeField]
    //Общая панель статистики
    private GameObject PanelStatistic = null;
    [SerializeField]
    //Родитель в который будут помещяться элементы статистики (вертикал скрол)
    private RectTransform PanelStatisticContent = null;
    [SerializeField]
    //Префаб элемента статистики 
    private GameObject PanelCardStatisticPrefab = null;

    //Мапа ячеек view
    private Dictionary<long, GameObject> CellsView = new Dictionary<long, GameObject>();
    //Мапа для палитры
    private Dictionary<int, GameObject> ColorsViewObjects = new Dictionary<int, GameObject>();

    //Предыдущий номер выбранного цвета (для его очистки Select)
    private int PrevSelectedColor = -1;

    //Добавление палитры на View
    public void GenerateColorsBar(Dictionary<int, Color32> ColorsDict)
    {
        foreach (var itemColor in ColorsDict)
        {
            InstantiateColorElement(itemColor.Key, itemColor.Value);
        }

        this.SelectColorShow(1);
    }

    //Создание и настройка 1 элемента палитры
    private void InstantiateColorElement(int numberColor, Color color)
    {
        GameObject colorElement = Instantiate(ColorElementPrefab);
        colorElement.GetComponentInChildren<Text>().text = numberColor.ToString();
        colorElement.GetComponent<Image>().color = color;
        colorElement.transform.SetParent(this.ContentColorBar, false);

        Button colorPick = colorElement.GetComponent<Button>();
        colorPick.onClick.AddListener(
            () => GameObject.FindGameObjectWithTag("GameController")
                  .GetComponent<MainGameController>()
                  .ColorClick(numberColor)
            );
        colorPick.onClick.AddListener(
            () => this.SelectColorShow(numberColor)
            );

        this.ColorsViewObjects.Add(numberColor, colorElement);
    }

    //Отображение при выборе цвета
    public void SelectColorShow(int numberColor)
    {
        if (this.PrevSelectedColor != numberColor)
        {
            if (this.ColorsViewObjects.TryGetValue(numberColor, out GameObject colorView))
            {
                this.ClearSelectColor();

                Color colorSelect = colorView.GetComponent<Image>().color;
                colorSelect.a = 0.5f;
                colorView.GetComponent<Image>().color = colorSelect;
                colorView.GetComponent<Image>().sprite = SelectedColorSprite;

                this.PrevSelectedColor = numberColor;
            }
        }
    }

    //Отображение когда цвет на поле закончился. Убирается в палитре его номер и нельзя выбрать
    public void ColorEnd(int numberColor)
    {
        if (this.ColorsViewObjects.TryGetValue(numberColor, out GameObject colorObject))
        {
            colorObject.GetComponent<Button>().interactable = false;
            colorObject.GetComponentInChildren<Text>().text = "";
        }
    }

    //Показ окна финальной статистики
    public void ShowFinalStatistics(List<string> statisticsMessage)
    {
        this.PanelStatistic.SetActive(true);

        foreach (var message in statisticsMessage)
        {
            GameObject statisticMessage = Instantiate(PanelCardStatisticPrefab);
            statisticMessage.transform.SetParent(this.PanelStatisticContent, false);

            statisticMessage.GetComponentInChildren<Text>().text = message;
        }
    }

    //Закрытие окна финальной статистики
    public void CloseWindowStatistic()
    {
        this.PanelStatistic.SetActive(false);
    }

    //Обновления статисики правильно закрашенных пикселей вверху справа
    public void UpdatePixelStatistics(int rightCount, int allCount)
    {
        TextPixelStats.text = rightCount.ToString() + " / " + allCount.ToString();
    }

    //Показ уведомления
    public void ShowNotify(string textNotify)
    {
        TextNotify.text = textNotify;
    }

    //Очистка предыдущего выбранного цвета палитры
    public void ClearSelectColor()
    {
        if (this.PrevSelectedColor > 0)
        {
            if (this.ColorsViewObjects.TryGetValue(this.PrevSelectedColor, out GameObject colorPrev))
            {
                Color prevColorSelected = colorPrev.GetComponent<Image>().color;
                prevColorSelected.a = 1;
                colorPrev.GetComponent<Image>().color = prevColorSelected;
                colorPrev.GetComponent<Image>().sprite = DefualtColorSprite;
            }
        }
    }

    //Конец игры
    public void GameOver()
    {
        this.ClearSelectColor();
        this.ShowNotify("Игра окончена");
    }

    //Создание вью элементов ячейки
    public void GenerateViewCells(Dictionary<long, int> colorsNeeded, int widthPicture, int heightPicture)
    {
        GridLayoutGroup grid = ScrollViewContent.GetComponent<GridLayoutGroup>();
        grid.constraintCount = widthPicture;

        foreach (var item in colorsNeeded)
        {
            InstantiateNewCell(item.Key, item.Value);
        }
    }

    //Создание 1 вью элемента ячейки и его настройка
    private void InstantiateNewCell(long numberCell, int numberNeededColor)
    {
        //Добавляем элемент на view
        GameObject cellView = Instantiate(CellPrefab);
        cellView.transform.SetParent(ScrollViewContent.GetComponent<RectTransform>(), false);
        cellView.GetComponentInChildren<Text>().text = numberNeededColor.ToString();

        Button button = cellView.GetComponent<Button>();
        button.onClick.AddListener(
            () => GameObject.FindGameObjectWithTag("GameController")
                .GetComponent<MainGameController>()
                .ClickCell(numberCell));

        EventTrigger trigger = cellView.GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerEnter;
        entry.callback.AddListener(
            (data) => {
            GameObject.FindGameObjectWithTag("GameController")
                .GetComponent<MainGameController>().ClickCell((PointerEventData)data, numberCell);
            });
        trigger.triggers.Add(entry);

        try
        {
            //Словарь объектов view
            CellsView.Add(numberCell, cellView);
        }
        catch (Exception exept)
        {
            Debug.Log(exept);
        }
    }

    //Очистить номер цвета в ячейке
    public void ClearText(long numberCell)
    {
        if (CellsView.TryGetValue(numberCell, out GameObject cellView))
        {
            cellView.GetComponentInChildren<Text>().text = "";
        }
    }

    //Правильное закрашивание цвета
    public void FillRight(long numberCell, Color32 colorFill)
    {
        if (CellsView.TryGetValue(numberCell, out GameObject cellView))
        {
            //убираю лишних слушателей и отключаю кнопку
            cellView.GetComponent<Button>().interactable = false;
            cellView.GetComponent<Button>().onClick.RemoveAllListeners();

            Image image = cellView.GetComponent<Image>();
            //убираем sprite, что бы целиком закрасился квадрат
            image.sprite = null;
            image.color = colorFill;
        }
    }

    //Неправильное закрашивание цвета
    public void FillFailer(long numberCell, Color32 colorFill)
    {
        if (CellsView.TryGetValue(numberCell, out GameObject cellView))
        {
            Image image = cellView.GetComponent<Image>();
            //устанавливаем чуть прозрачность
            colorFill.a = 125;
            image.color = colorFill;
        }
    }

    //Увеличение
    public void ZoomPlus()
    {
        GridLayoutGroup gridLayoutGroup = this.ScrollViewContent.GetComponent<GridLayoutGroup>();
        this.CellSizeNow = Mathf.Clamp(this.CellSizeNow + this.DeltaSize, this.MinCellSize, this.MaxCellSize);
        gridLayoutGroup.cellSize = new Vector2(this.CellSizeNow, this.CellSizeNow);
        CheckButtonsZoomEnabled();
    }

    //Уменьшение
    public void ZoomMinus()
    {
        GridLayoutGroup gridLayoutGroup = this.ScrollViewContent.GetComponent<GridLayoutGroup>();
        this.CellSizeNow = Mathf.Clamp(this.CellSizeNow - this.DeltaSize, this.MinCellSize, this.MaxCellSize);
        gridLayoutGroup.cellSize = new Vector2(this.CellSizeNow, this.CellSizeNow);
        CheckButtonsZoomEnabled();
    }

    //Проверка на активность кнопок увеличения/уменьшения
    private void CheckButtonsZoomEnabled()
    {
        if (this.CellSizeNow == this.MaxCellSize)
        {
            this.ButtonZoomPlus.GetComponent<Button>().interactable = false;
        }
        else
        {
            this.ButtonZoomPlus.GetComponent<Button>().interactable = true;
        }
        if (this.CellSizeNow == this.MinCellSize)
        {
            this.ButtonZoomMinus.GetComponent<Button>().interactable = false;
        }
        else
        {
            this.ButtonZoomMinus.GetComponent<Button>().interactable = true;
        }
        if (this.CellSizeNow <= this.MinForHideNumberColor && this.ShowNumberColor)
        {
            this.ShowNumberColor = false;
            HideShowText(this.ShowNumberColor);
        }
        else
        {
            if (!this.ShowNumberColor && this.CellSizeNow >= this.MinForHideNumberColor)
            {
                this.ShowNumberColor = true;
                HideShowText(this.ShowNumberColor);
            }
        }
    }

    //Показывать/не показывать номер цвета в ячейке
    private void HideShowText(bool ShowText)
    {
        foreach (var item in this.CellsView)
        {
            item.Value.GetComponentInChildren<Text>().enabled = ShowText;
        }
    }

    //показать историю закрашивания
    public void ShowHistoryPaint(List<KeyValuePair<long, int>> HistoryCell,
        Dictionary<int, Color32> colorsDict,
        List<string> statisticsMessage = null)
    {
        //сбрасываем со всех ячеек цвет
        foreach (var cellView in CellsView)
        {
            cellView.Value.GetComponent<Image>().color = Color.white;
        }

        StartCoroutine(this.SettingZoomAndShowHistory(HistoryCell, colorsDict, statisticsMessage));
    }

    //Настройка зума. Отдаляем картинку до тех пока пока не исчезнут sroll bar-ы 
    //или не достигнется минимального значения размера одной ячейки
    private IEnumerator SettingZoomAndShowHistory(List<KeyValuePair<long, int>> HistoryCell,
        Dictionary<int, Color32> colorsDict,
        List<string> statisticsMessage)
    {
        //Натсраиваю масштаю картинки, что бы она показывалась целиком
        while (this.HorizontalScrollBar.activeSelf || this.VerticalScrollBar.activeSelf)
        {
            if (this.CellSizeNow == this.MinCellSize)
            {
                break;
            }
            this.ZoomMinus();

            yield return new WaitForEndOfFrame();
        }

        StartCoroutine(this.ShowHistory(HistoryCell, colorsDict, statisticsMessage));
    }
    
    //показать историю закрашивания
    private IEnumerator ShowHistory(List<KeyValuePair<long, int>> HistoryCell,
        Dictionary<int, Color32> colorsDict,
        List<string> statisticsMessage)
    {
        foreach (var item in HistoryCell)
        {
            if (CellsView.TryGetValue(item.Key, out GameObject cellView))
            {
                cellView.GetComponent<Image>().color = colorsDict[item.Value];
            }
            yield return new WaitForSeconds(SettingsClass.DelayShowHistory);
        }

        this.ShowFinalStatistics(statisticsMessage);
    }
}