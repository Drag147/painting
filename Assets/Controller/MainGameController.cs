﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MainGameController : MonoBehaviour, IObserver
{
    //Чекает долго нажатие, и переключает 2 состояние курсора
    //чтобы была возможность закрашивать просто водя мышкой с зажатой кнопкой
    public Control control;
    //Вьюха
    public View2DUI view;
    //Моделька
    public UsualGameModel usualGameModel = new UsualGameModel();
    //Ассемблер, который перегоняет (или генерирует случайное) изображение в ImageInfo,
    //где хранится палитра, и номер пикселя и его цвета
    //Умеет загружать изображение по прямой ссылке из интернета, либо генерировать случайную (ничего не значащюю картинку)
    public ImageAssemblerBase imageAssemblerBase;
    //Мапа K = номер цвета, V = Цвет в byte формате, 
    //так как int удобнее сравнивать и можно реализовать отсеивание ненужны цветов
    private Dictionary<int, Color32> ColorsDict = null;

    //Текущий выбранный цвет "кисти"
    private int nowColorNumber = 1;
    //Флаг конца игры
    private bool GameOver = false;

    void Start()
    {
        if (!SettingsClass.EnableQuickPainting)
        {
            GameObject.Destroy(control);
        }
        else
        {
            control.enableDragClick = SettingsClass.EnableQuickPainting;
            control.holdTime = SettingsClass.TimeHoldButton;
        }

        StartCoroutine(this.InitiateField());
    }

    //Инициализация игрового поля
    private IEnumerator InitiateField()
    {
        //Класс распарсенной картинки
        ImageInfoForUsualCell imageInfo = null;
        if (SettingsClass.GenerateImage)
        {
            imageInfo = imageAssemblerBase.GenerateRandomImage(SettingsClass.Width, SettingsClass.Height, SettingsClass.NumberColor);
        }
        else
        {
            StartCoroutine(imageAssemblerBase.LoadImageFromUrl(SettingsClass.UrlLoad, SettingsClass.Different));
            if (imageAssemblerBase.imageInfo == null)
            {
                yield return new WaitForSeconds(1.5f);
            }

            if (imageAssemblerBase.imageInfo == null)
            {
                imageInfo = imageAssemblerBase.GenerateRandomImage(SettingsClass.Width, SettingsClass.Height, SettingsClass.NumberColor);
            }
            else
            {
                imageInfo = imageAssemblerBase.imageInfo;
            }
        }
        this.ColorsDict = imageInfo.ColorsDictionary;
        this.usualGameModel.InitCell(imageInfo.CellsDict);
        this.usualGameModel.InitColorsLeft(imageInfo.ColorNumbersLeft);
        this.usualGameModel.AddOserver(this);

        this.view.UpdatePixelStatistics(this.usualGameModel.GetPixelStatisticRight().Key, this.usualGameModel.GetPixelStatisticRight().Value);

        this.view.GenerateColorsBar(this.ColorsDict);
        this.view.GenerateViewCells(imageInfo.NeededColors, imageInfo.PixelWidthImage, imageInfo.PixelHeightImage);
    }

    //Клик по ячейке
    public void ClickCell(long numberCell)
    {
        //Приходит ResultClick, enum с результатом клика
        switch (usualGameModel.ClickCell(numberCell, this.nowColorNumber))
        {
            case ResultClick.Right:
                view.ClearText(numberCell);
                //Закрашиваем квадрат цветом, который был выделен
                view.FillRight(numberCell, this.ColorsDict[this.nowColorNumber]);
                break;
            case ResultClick.Failure:
                //Закрашиваем квадрат цветом, который был выделен  
                view.FillFailer(numberCell, this.ColorsDict[this.nowColorNumber]);
                break;
            case ResultClick.None:
                //пустой, ничего не требуется при нажатии на правильно закрашенные цвет
                break;
            case ResultClick.Error:
                Debug.Log("Error in click.");
                break;
            default:
                Debug.Log("Error NO ResultClick enum.");
                break;
        }

        if (this.GameOver && SettingsClass.ShowHistory)
        {
            this.view.ShowHistoryPaint(
                usualGameModel.GetHistoryList(),
                this.ColorsDict,
                usualGameModel.GetFinalStatistics());
        }
        else if (this.GameOver && !SettingsClass.ShowHistory)
        {
            this.view.ShowFinalStatistics(usualGameModel.GetFinalStatistics());
        }
    }

    //Клик по ячейке при зажатой левой кнопке. Реализовано через EventTrigger
    public void ClickCell(PointerEventData data, long numberCell)
    {
        if (control.Draged && control.enableDragClick)
        {
            ClickCell(numberCell);
        }
    }

    //Выбор цвета
    public void ColorClick(int numberColor)
    {
        this.nowColorNumber = numberColor;
    }

    //События позволяющие обновлять статистику на экране пользователя, в частности Кол-во правильно закрашенных пикселей
    public void UpdateObserver(Statistic.EventUpdate eventUpdate)
    {
        switch (eventUpdate)
        {
            case Statistic.EventUpdate.GameOver:
                this.view.GameOver();
                this.view.ShowNotify(usualGameModel.GetSummaryTime());
                //Пришлось добавить флаг, для нормальной работы истории, а иначе ClickCell не успевал до конца отработать
                //и последняя клетка не закрашивалась, а Событие GameOver уже вызывалось, и ломалась курутина, поскольку
                //в лист History добавлялся новый элемент
                this.GameOver = true;
                break;
            case Statistic.EventUpdate.ColorLeft:
                this.view.ColorEnd(this.nowColorNumber);
                break;
            case Statistic.EventUpdate.UpdatePixelCount:
                this.view.UpdatePixelStatistics(usualGameModel.GetPixelStatisticRight().Key,
                    usualGameModel.GetPixelStatisticRight().Value);
                break;
            case Statistic.EventUpdate.StatisticsReady:
                break;
            default:
                break;
        }
    }
}
