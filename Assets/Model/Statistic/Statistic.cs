﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class Statistic : IObservable
{
    //Типы Уведомлений посылаемые контроллеру
    public enum EventUpdate
    {
        GameOver,
        ColorLeft,
        UpdatePixelCount,
        StatisticsReady
    }
    
    //Создаётся в моделе и вызывает Notify контрллера при достижении необходимых условий через подписку (обсервер)

    //кол-во всех пикселей
    public int NumberAllPixel { get; set; }
    //кол-во правильно закрашенных
    public int NumberRightPixel { get; set; }
    //кол-во неправильно закрашенных
    public int NumberFailurePixel { get; set; }
    //Мапа с K = номер цвета, V = кол-во оставшихся ячеек для закрашивания
    public Dictionary<int, int> ColorsNumberLeft { get; set; }
    //Мапа с подписчиками
    private SortedSet<IObserver> Observers = new SortedSet<IObserver>();
    //Общая статистика
    public List<string> StatisticsMessage { get; private set; }

    //Общее затраченное время
    public string SummaryTimeMessage;
    private DateTime StartTime;

    public Statistic(int NumberAllPixel)
    {
        this.StatisticsMessage = new List<string>(1);

        this.NumberAllPixel = NumberAllPixel;
        this.NumberRightPixel = 0;

        StartTime = DateTime.Now;
    }

    public void InitColorsLeft (Dictionary<int, int> ColorsNumberLeft)
    {
        this.ColorsNumberLeft = ColorsNumberLeft;
    }

    public void AddObserver(IObserver observer)
    {
        Observers.Add(observer);
    }

    public void NotifyObservers(EventUpdate eventUpdate)
    {
        foreach (var Observer in Observers)
        {
            Observer.UpdateObserver(eventUpdate);
        }
    }

    public void RemoveObserver(IObserver observer)
    {
        Observers.Remove(observer);
    }

    public void RemoveColorLeft(int ColorNumber)
    {
        if (this.ColorsNumberLeft.TryGetValue(ColorNumber, out int value))
        {
            this.ColorsNumberLeft[ColorNumber]--;
            if (this.ColorsNumberLeft[ColorNumber] <= 0)
            {
                this.NotifyObservers(EventUpdate.ColorLeft);
            }
        }
    }

    public void RightPixelClick()
    {
        this.NumberRightPixel = Mathf.Clamp(this.NumberRightPixel+1, 0, this.NumberAllPixel);
        this.NotifyObservers(EventUpdate.UpdatePixelCount);
        if (this.NumberAllPixel == this.NumberRightPixel)
        {
            this.CalculateSummaryTime();
            this.NotifyObservers(EventUpdate.GameOver);
            this.GenerateStatistics();
        }
    }

    public void FailurePixelClick()
    {
        this.NumberFailurePixel++;
    }

    public string GetSummaryTimeForNotify()
    {
        return "Общее время " + this.SummaryTimeMessage;
    }

    private void CalculateSummaryTime()
    {
        TimeSpan time = DateTime.Now.Subtract(this.StartTime);
        this.SummaryTimeMessage = "";
        if (time.Hours != 0)
        {
            this.SummaryTimeMessage += time.Hours.ToString() + ":";
        }
        this.SummaryTimeMessage += time.Minutes.ToString().Length != 2 ? "0" + time.Minutes.ToString()
            : time.Minutes.ToString();
        this.SummaryTimeMessage += ":";
        this.SummaryTimeMessage += time.Seconds.ToString().Length != 2 ? "0" + time.Seconds.ToString()
            : time.Seconds.ToString();
    }

    //Собираем всю статистику и кидаем событие StatisticsReady
    private void GenerateStatistics()
    {
        this.StatisticsMessage.Clear();

        this.StatisticsMessage.Add("Количество ошибочных закрашиваний: " + this.NumberFailurePixel);
        this.StatisticsMessage.Add("Общее время закрашивания: " + this.SummaryTimeMessage);
        //Остальная статистика любая

        this.NotifyObservers(EventUpdate.StatisticsReady);
    }
}