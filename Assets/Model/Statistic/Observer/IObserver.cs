﻿using UnityEngine;

//Наблюдатель
public interface IObserver
{
    void UpdateObserver(Statistic.EventUpdate eventUpdate);
}
