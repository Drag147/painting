﻿
//Наблюдаемый объект
public interface IObservable
{
    void AddObserver(IObserver observer);
    void NotifyObservers(Statistic.EventUpdate eventUpdate);
    void RemoveObserver(IObserver observer);
}
