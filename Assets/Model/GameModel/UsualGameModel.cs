﻿using System.Collections.Generic;

public class UsualGameModel : GameModel
{
    protected Dictionary<long, UsualCell> CellsDict;
    //Отдельный класс для посчёта любой статистики. Отправляет события в MainGameController для обновления вьюхи.
    //Можно туда добавить подсчёт в принципе любой статистики, времени не так много (так как диплом) для реализации
    //всех статистик какие придут в голову, поэтому вот парочку сделал с подсчётом общего затраченного времени на раскрашивание,
    //кол-во неверно закрашенных пикселей и кол-во верных закрашенных с обновлением в реальном времени,
    //также считает сколько осталось ячеек с конкретным цветом и при закрашивании всех ячеек одного цвета кидает ColorLeft
    //который отключает цвет для выбора
    protected Statistic statistic;
    //Класс истории, сохраняет все (так сейчас сделано, при желании легко изменить) закрашенные ячейки
    protected History history;

    public UsualGameModel()
    {

    }

    //Добавить наблюдатель. Тут пока 1 только главный контроллер
    public void AddOserver(IObserver observer)
    {
        this.statistic.AddObserver(observer);
    }

    //Создание ячейки у которой есть Номер (уникальный идентификатор) и Требуемый цвет.
    //Наследуется от базовой Cell. Есть возможность для создания новых видов ячеек.
    public void InitCell(Dictionary<long, UsualCell> CellsDict)
    {
        this.CellsDict = CellsDict;
        this.statistic = new Statistic(CellsDict.Count);
        this.history = new History(CellsDict.Count);
    }

    //запоминаем цвета и их кол-во на поле
    public void InitColorsLeft(Dictionary<int, int> ColorsNumberLeft)
    {
        this.statistic.InitColorsLeft(ColorsNumberLeft);
    }

    //клик по ячейке. Реализован через State. читайте в State
    public ResultClick ClickCell(long numberCell, int ColorNumber)
    {
        if (CellsDict.TryGetValue(numberCell, out UsualCell usualCell))
        {
            ResultClick resultClick = usualCell.ClickSell(ColorNumber);
            if (resultClick == ResultClick.Right)
            {
                statistic.RemoveColorLeft(ColorNumber);
                statistic.RightPixelClick();
            }
            if (resultClick == ResultClick.Failure)
            {
                statistic.FailurePixelClick();
            }
            if (resultClick == ResultClick.Failure || resultClick == ResultClick.Right)
            {
                this.history.AddCell(numberCell, ColorNumber);
            }
            return resultClick;
        }
        return ResultClick.Error;
    }

    //Статистика правильно закрашенных ячеек
    public KeyValuePair<int, int> GetPixelStatisticRight()
    {
        return new KeyValuePair<int, int>(statistic.NumberRightPixel, statistic.NumberAllPixel);
    }

    //общее время закрашивания картинки
    public string GetSummaryTime()
    {
        return this.statistic.GetSummaryTimeForNotify();
    }

    //Вся статистика
    public List<string> GetFinalStatistics()
    {
        return this.statistic.StatisticsMessage;
    }

    //Получение истории кликов по ячейкам
    public List<KeyValuePair<long, int>> GetHistoryList()
    {
        return this.history.HistoryCell;
    }
}
