﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class ImageAssemblerBase : MonoBehaviour
{
    public ImageInfoForUsualCell imageInfo { get; private set; }
    private float delta = 9f;
    //formula = 10 - different
    //9 - очень легко
    //8 - легко
    //7 - средне
    //6 - тяжело
    //5 - очень тяжело

    public ImageInfoForUsualCell GenerateRandomImage(int pixelWidth, int pixelHeight, int numberColors)
    {
        imageInfo = new ImageInfoForUsualCell(pixelWidth, pixelHeight);
        long numberNewCell = 1;
        GenerateColors(numberColors);
        for (int x = 0; x < pixelWidth; x++)
        {
            for (int y = 0; y < pixelHeight; y++)
            {
                imageInfo.addCell(numberNewCell, new UsualCell(numberNewCell, Random.Range(1, numberColors + 1)));
                numberNewCell++;
            }
        }

        return imageInfo;
    }

    private void GenerateColors(int numberColors)
    {
        for (int i = 0; i < numberColors;)
        {
            Color32 color = new Color32((byte)Random.Range(0, 255), (byte)Random.Range(0, 255), (byte)Random.Range(0, 255), 255);
            imageInfo.AddColorInDict(color, out int numberColor);
            if (numberColor != -1)
            {
                i++;
            }
        }
    }

    public IEnumerator LoadImageFromUrl(string url, int different)
    {
        if(url != "" && url != null)
        {
            using (UnityWebRequest www = UnityWebRequestTexture.GetTexture(url))
            {
                yield return www.SendWebRequest();

                if (www.isNetworkError || www.isHttpError)
                {
                    Debug.Log(www.error);
                }
                else
                {
                    Texture2D UploadTexture = DownloadHandlerTexture.GetContent(www);
                    this.GenerateImageInfoForModel(UploadTexture, different);
                }
            }
        }
    }

    public void GenerateImageInfoForModel(Texture2D UploadTexture, int different)
    {
        this.delta = 10 - different;
        imageInfo = null;
        if (UploadTexture != null)
        {
            imageInfo = new ImageInfoForUsualCell(UploadTexture.width, UploadTexture.height);
            long numberNewCell = 1;
            int numberColor = 1;
            Color32[] colors = UploadTexture.GetPixels32();
            for (int i = 0; i < colors.Length; i++)
            {
                if (Diff(colors[i], out numberColor))
                {
                    imageInfo.AddColorInDict(colors[i], out numberColor);
                }
                imageInfo.addCell(numberNewCell, new UsualCell(numberNewCell, numberColor));
                numberNewCell++;
            }
        }
    }

    private bool Diff(Color32 a, out int numberColor)
    {
        numberColor = -1;
        foreach (var item in this.imageInfo.ColorsDictionary)
        {
            Color32 b = item.Value;
            float diff = new Vector3(Mathf.Abs(0.59f * (a.r - b.r)),
                            Mathf.Abs(0.3f * (a.g - b.g)),
                            Mathf.Abs(0.11f * (a.b - b.b)))
                                .magnitude;
            if (diff <= this.delta)
            {
                numberColor = item.Key;
                return false;
            }
        }
        return true;
    }
}
