﻿using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class ImageInfo
{
    public Dictionary<long, int> NeededColors { get; private set; }
    public Dictionary<int, Color32> ColorsDictionary { get; private set; }
    public Dictionary<int, int> ColorNumbersLeft { get; private set; }

    public int PixelWidthImage { get; protected set; }
    public int PixelHeightImage { get; protected set; }

    public int NumberColorNow {get; private set;}

    public ImageInfo()
    {
        this.ColorsDictionary = new Dictionary<int, Color32>();
        this.NeededColors = new Dictionary<long, int>();
        this.ColorNumbersLeft = new Dictionary<int, int>();
        this.NumberColorNow = 1;
    }

    public void AddColorInDict(Color32 color, out int numberColor)
    {
        numberColor = -1;
        try
        {
            if (!this.ColorsDictionary.ContainsValue(color))
            {
                ColorsDictionary.Add(NumberColorNow, color);
                numberColor = NumberColorNow;
                NumberColorNow++;
            }
        }
        catch (Exception exept)
        {
            Debug.Log(exept);
        }
    }

    public void AddColorInDict(Color32 color)
    {
        try
        {
            if (!this.ColorsDictionary.ContainsValue(color))
            {
                ColorsDictionary.Add(NumberColorNow, color);
                NumberColorNow++;
            }
        }
        catch (Exception exept)
        {
            Debug.Log(exept);
        }
    }

    public int FindKeyByValue(Color32 color)
    {
        foreach (var item in this.ColorsDictionary)
        {
            if (item.Value.Equals(color))
            {
                return item.Key;
            }
        }
        return -1;
    }
}
