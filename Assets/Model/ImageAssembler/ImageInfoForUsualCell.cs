﻿using System.Collections.Generic;

public class ImageInfoForUsualCell : ImageInfo
{
    public Dictionary<long, UsualCell> CellsDict { get; private set; }

    public ImageInfoForUsualCell(int pixelWidthImage, int pixelHeightImage)
    {
        this.CellsDict = new Dictionary<long, UsualCell>();
        this.PixelWidthImage = pixelWidthImage;
        this.PixelHeightImage = pixelHeightImage;
    }

    public void addCell(long numberCell, UsualCell cell)
    { 
        CellsDict.Add(numberCell, cell);
        NeededColors.Add(numberCell, cell.ColorNeededNumber);

        if(this.ColorNumbersLeft.TryGetValue(cell.ColorNeededNumber, out int value))
        {
            this.ColorNumbersLeft[cell.ColorNeededNumber]++;
        }
        else
        {
            this.ColorNumbersLeft.Add(cell.ColorNeededNumber, 1);
        }
    }
}
