﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class History
{
    //Список для добавления истории кликов по ячейкам Номер ячейки, номер цвета ячейки
    //Все клики по ячейкам будут лежать тут
    public List<KeyValuePair<long, int>> HistoryCell { get; }

    public History(int capacity)
    {
        this.HistoryCell = new List<KeyValuePair<long, int>>(capacity*2);
    }

    public void AddCell(long number, int numberColor)
    {
        this.HistoryCell.Add(new KeyValuePair<long, int>(number, numberColor));
    }
}
