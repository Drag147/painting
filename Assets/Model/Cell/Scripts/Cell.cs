﻿public enum ResultClick
{
    Right,
    Failure,
    None,
    Error
}

//Общий класс для ячейки
public abstract class Cell
{
    //Номер ячейки для её идентификации
    public long NumberCell { get; protected set; }
    //Номер нужного цвета
    public int ColorNeededNumber { get; set; }

    public abstract ResultClick ClickSell(int NewNumberColor);
}
