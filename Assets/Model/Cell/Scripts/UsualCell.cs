﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Обычная ячейки. 
//Клик = закрашивание, если её состояние было неправильным или она была чистой.
public class UsualCell : Cell
{
    public int ColorNowNumber { get; set; }

    /* Состояния ячейки:
     * 1) Ячейка не закрашена (StateClear);
     * 2) Ячейка закрашена Правильно (StateRight)
     * 3) Ячейка закрашена Не правильно (StateFailure) 
     */
    public StateCell StateCell { get; set; }

    public UsualCell(long NumberCell, int ColorNeededNumber)
    {
        this.NumberCell = NumberCell;
        this.ColorNeededNumber = ColorNeededNumber;
        this.ColorNowNumber = -1;

        this.StateCell = StateUsualCellClear.getClearState();
    }

    public override ResultClick ClickSell(int NewNumberColor)
    {
        return this.StateCell.ClickCell(this, NewNumberColor);
    }
}
