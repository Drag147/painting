﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Состояние ячейки когда она закрашена неправильно.
public class StateUsualCellFailure : StateCell
{
    private static StateUsualCellFailure stateCellFailure = null;

    private StateUsualCellFailure()
    {

    }

    public override ResultClick ClickCell(Cell cell, int NewNumberColor)
    {
        try
        {
            UsualCell usualCell = (UsualCell)cell;
            CheckClickColor(usualCell.ColorNeededNumber, NewNumberColor, out ResultClick result);
            if (result == ResultClick.Right)
            {
                usualCell.ColorNowNumber = NewNumberColor;
                usualCell.StateCell = StateUsualCellRight.getRightState();
            }
            return result;
        }
        catch { return ResultClick.Error; }
    }
    
    //Вынес проверку в отдельный метод, вдруг ктото потом захочет её изменить
    private void CheckClickColor(int ColorNeededNumber, int NewNumberColor, out ResultClick result)
    {
        if (NewNumberColor == ColorNeededNumber)
        {
            result = ResultClick.Right;
            return;
        }
        result = ResultClick.Failure;
    }

    public static StateUsualCellFailure getFailureState()
    {
        if (stateCellFailure == null)
        {
            stateCellFailure = new StateUsualCellFailure();
        }
        return stateCellFailure;
    }
}
