﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Состояние ячейки когда она не закрашена.
public class StateUsualCellClear : StateCell
{
    private static StateUsualCellClear stateCellClear = null;

    private StateUsualCellClear()
    {

    }

    public static StateUsualCellClear getClearState()
    {
        if (stateCellClear == null)
        {
            stateCellClear = new StateUsualCellClear();
        }
        return stateCellClear;
    }

    public override ResultClick ClickCell(Cell cell, int NewNumberColor)
    {
        try
        {
            UsualCell usualCell = (UsualCell)cell;
            CheckClickColor(usualCell.ColorNeededNumber, NewNumberColor, out ResultClick result);
            if (result == ResultClick.Right)
            {
                usualCell.StateCell = StateUsualCellRight.getRightState();
            }
            else
            {
                usualCell.StateCell = StateUsualCellFailure.getFailureState();
            }
            usualCell.ColorNowNumber = NewNumberColor;
            return result;
        }
        catch { return ResultClick.Error; }
    }

    //Вынес проверку в отдельный метод, вдруг ктото потом захочет её изменить
    private void CheckClickColor(int ColorNeededNumber, int NewNumberColor, out ResultClick result)
    {
        if (NewNumberColor == ColorNeededNumber)
        {
            result = ResultClick.Right;
            return;
        }
        result = ResultClick.Failure;
    }
}
