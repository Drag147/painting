﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Состояние ячейки когда она закрашена правильно.
public class StateUsualCellRight : StateCell
{
    private static StateUsualCellRight thisState = null;

    private StateUsualCellRight()
    {
    }

    public override ResultClick ClickCell(Cell cell, int NewNumberColor)
    {
        //Если кликнули по ячейке, которая уже закрашена в правильный цвет то ничего не делаем
        return ResultClick.None;
    }

    //Делаю классы состояние singleton, чтобы имели только 1 реализацию
    public static StateUsualCellRight getRightState()
    {
        if (thisState == null)
        {
            thisState = new StateUsualCellRight();
        }
        return thisState;
    }
}
