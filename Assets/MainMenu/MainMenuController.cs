﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    public GameObject panelWithWidthAndHight;
    public GameObject panelWithUrl;

    public Text textToggle;
    public string textToggleWhenWandH = "Случайное изображение";
    public string textToggleWhenUrl = "Изображение из интернета";

    public Text textDiff;
    public string textDiff1 = "Очень легко";
    public string textDiff2 = "Легко";
    public string textDiff3 = "Средне";
    public string textDiff4 = "Тяжело";
    public string textDiff5 = "Очень тяжело";

    public Toggle toggle;
    public InputField inputFieldW;
    public InputField inputFieldH;
    public InputField inputFieldNumberColor;
    public InputField inputFieldUrl;
    public Slider slider;

    public Toggle toggleQuickPainting;
    public InputField inputFieldHoldTime;
    
    public void ChangePanelSettingImage(bool isActiveCheckBox)
    {
        if (isActiveCheckBox)
        {
            textToggle.text = textToggleWhenWandH;
            panelWithUrl.SetActive(false);
            panelWithWidthAndHight.SetActive(true);
        }
        else
        {
            textToggle.text = textToggleWhenUrl;
            panelWithUrl.SetActive(true);
            panelWithWidthAndHight.SetActive(false);
        }
    }

    public void ChangeQuickPaint(bool isActiveQuickPaint)
    {
        if (isActiveQuickPaint)
        {
            inputFieldHoldTime.interactable = true;
        }
        else
        {
            inputFieldHoldTime.interactable = false;
        }
    }

    public void ChangeDifferent(Single valueDiff)
    {
        switch (valueDiff)
        {
            case 1:
                textDiff.text = textDiff1;
                break;
            case 2:
                textDiff.text = textDiff2;
                break;
            case 3:
                textDiff.text = textDiff3;
                break;
            case 4:
                textDiff.text = textDiff4;
                break;
            case 5:
                textDiff.text = textDiff5;
                break;
            default:
                break;
        }
    }

    public void PlayClick()
    {
        SettingsClass.Width = Convert.ToInt32(inputFieldW.text);
        SettingsClass.Height = Convert.ToInt32(inputFieldH.text);
        SettingsClass.NumberColor = Convert.ToInt32(inputFieldNumberColor.text);
        SettingsClass.Different = Convert.ToInt32(slider.value);
        if (toggle.isOn)
        {
            SettingsClass.GenerateImage = true;
            SettingsClass.UrlLoad = null;
            
        }
        else
        {
            SettingsClass.GenerateImage = false;
            SettingsClass.UrlLoad = inputFieldUrl.text;
        }

        SettingsClass.EnableQuickPainting = toggleQuickPainting.isOn;
        SettingsClass.TimeHoldButton = Convert.ToSingle(inputFieldHoldTime.text, new System.Globalization.CultureInfo("en-US"));

        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
