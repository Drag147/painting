﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuController : MonoBehaviour
{
    public Canvas mainCanvas;
    public Canvas menuCanvas;

    public void Pause()
    {
        Time.timeScale = 0;
        this.menuCanvas.enabled = true;
        this.mainCanvas.enabled = false;
    }

    public void Resume()
    {
        Time.timeScale = 1;
        this.menuCanvas.enabled = false;
        this.mainCanvas.enabled = true;
    }

    public void GoMainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
